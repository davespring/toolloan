﻿$(document).ready(function () {


    console.log("SignUp Form Validation Initialized");

    $("#signUpForm").validate({
        rules: {
            UserName: { required: true, minlength: 5, maxlength: 50 },
            Password: { required: true, strongPassword: true },
            confirmPassword: { equalTo: "#password" },
            DriversLicenseNumber: { required: true, minlength: 8, maxlength: 8 },
            PhoneNumber: { required: true, phoneUs: true, minlength: 10, maxlength: 10 }
        },

        messages: {
            UserName: { required: "A username is required", minlength: "Not a valid username. Must be at least 5 characters." },
            DriversLicenseNumber: { required: "A drivers license number is required", minlength: "Not a valid drivers license. Must be  exactly 8 digits", maxlength: "Not a valid drivers license. Must be  exactly 8 digits" },
            PhoneNumber: { required: "A phone number is required", phoneUs: "Not a valid US phone number.", minlength: "Phone number must be exactly 10 digits", maxlength: "Phone number must be exactly 10 digits" },
        },

        highlight: function (element) {
            $(element).closest('.form-group').addClass('has-error').removeClass('has-success');
        },

        unhighlight: function (element) {
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
        },

        errorElement: 'span',
        errorClass: 'help-block',
        errorPlacement: function (error, element) {
            if (element.parent('.input-group').length) {
                error.insertAfter(element.parent());
            } else {
                error.insertAfter(element);
            }
        },

        validClass: "has-success"

    });

    $.validator.addMethod("strongPassword", function (value, index) {
        var regex = /^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$/;
        return value.match(regex);
    }, "Must be 8 characters, one uppercase, one lowercase, and one special character");



});