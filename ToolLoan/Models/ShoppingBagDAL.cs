﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ToolLoan.Data.DAL;
using ToolLoan.Data.Models;

namespace ToolLoan.Models
{
    public class ShoppingBagDAL
    {
        private ToolLoanContext db = new ToolLoanContext();

        string ShoppingBagId { get; set; }
        public const string CartSessionKey = "CartId";


        // Get Cart using GetCartId() method
        public static ShoppingBagDAL GetCart(HttpContextBase context)
        {
            var cart = new ShoppingBagDAL();
            cart.ShoppingBagId = cart.GetCartId(context);
            return cart;
        }


        // Helper method to simplify shopping cart calls
        public static ShoppingBagDAL GetCart(Controller controller)
        {
            return GetCart(controller.HttpContext);
        }

        public void AddToCart(Tool Tool)
        {
            // Get the matching cart and Tool instances
            var cartItem = db.Carts.SingleOrDefault(
                c => c.CartId == ShoppingBagId
                && c.ToolId == Tool.ToolId);

            if (cartItem == null)
            {
                // Create a new cart item if no cart item exists
                cartItem = new Cart
                {
                    ToolId = Tool.ToolId,
                    CartId = ShoppingBagId,
                    Count = 1,
                    DateCreated = DateTime.Now
                };
                db.Carts.Add(cartItem);
            }
            else
            {
                // If the item does exist in the cart, 
                // then add one to the quantity
                cartItem.Count++;
            }
            // Save changes
            db.SaveChanges();
        }


        public int RemoveFromCart(int id)
        {
            // Get the cart
            var cartItem = db.Carts.Single(
                cart => cart.CartId == ShoppingBagId
                && cart.RecordId == id);

            int itemCount = 0;

            if (cartItem != null)
            {
                if (cartItem.Count > 1)
                {
                    cartItem.Count--;
                    itemCount = cartItem.Count;
                }
                else
                {
                    db.Carts.Remove(cartItem);
                }
                // Save changes
                db.SaveChanges();
            }
            return itemCount;
        }

        public void EmptyCart()
        {
            var cartItems = db.Carts.Where(
                cart => cart.CartId == ShoppingBagId);

            foreach (var cartItem in cartItems)
            {
                db.Carts.Remove(cartItem);
            }
            // Save changes
            db.SaveChanges();
        }

        
        public List<Cart> GetCartItems()
        {
            return db.Carts.Where(
                cart => cart.CartId == ShoppingBagId).ToList();
        }


        public int GetCount()
        {
            // Get the count of each item in the cart and sum them up
            int? count = (from cartItems in db.Carts
                          where cartItems.CartId == ShoppingBagId
                          select (int?)cartItems.Count).Sum();
            
            // Return 0 if all entries are null
            return count ?? 0;
        }


        // Create the order from the cartItems
        // Needed for use in the checkout process
        public int CreateOrder(Order order)
        {
            

            var cartItems = GetCartItems();
            
            // Iterate over the items in the cart, 
            // adding the order details for each.
            // Using (OrderId, Username, and OrderDate)
            // from order object passed in.
            foreach (var item in cartItems)
            {
                var orderDetail = new OrderDetail
                {
                    ToolId = item.ToolId,
                    OrderId = order.OrderId,
                    Quantity = item.Count,
                    UserName = order.Username,
                    LoanStartDate = order.OrderDate,
                    LoanEndDate = DateTime.Now.AddDays(7)
                };

                db.OrderDetails.Add(orderDetail);

            }

            // Save the order
            db.SaveChanges();
            // Empty the shopping cart
            EmptyCart();
            // Return the OrderId as the confirmation number
            return order.OrderId;
        }


        // We're using HttpContextBase to allow access to cookies.
        public string GetCartId(HttpContextBase context)
        {
            if (context.Session[CartSessionKey] == null)
            {
                if (!string.IsNullOrWhiteSpace(context.User.Identity.Name))
                {
                    context.Session[CartSessionKey] =
                        context.User.Identity.Name;
                        
                }
                else
                {
                    // Generate a new random GUID using System.Guid class
                    Guid tempCartId = Guid.NewGuid();
                    
                    // Send tempCartId back to client as a cookie
                    context.Session[CartSessionKey] = tempCartId.ToString();
                }
            }
            return context.Session[CartSessionKey].ToString();
        }


        // When a user has logged in, migrate their shopping cart to
        // be associated with their username
        public void MigrateCart(string userName)
        {
            var ShoppingBag = db.Carts.Where(
                c => c.CartId == ShoppingBagId);

            foreach (Cart item in ShoppingBag)
            {
                item.CartId = userName;
            }
            db.SaveChanges();
        }
    }
}