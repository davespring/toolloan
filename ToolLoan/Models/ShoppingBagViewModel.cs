﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using ToolLoan.Data.Models;

namespace ToolLoan.Models
{
    public class ShoppingBagViewModel
    {
        [Key]
        public List<Cart> CartItems { get; set; }

    }
}