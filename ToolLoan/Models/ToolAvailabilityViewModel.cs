﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ToolLoan.Models
{
    public class ToolAvailabilityViewModel
    {
       public DateTime ToolsEarliestAvailability { get; set; }
       public Data.Models.Tool Tool { get; set; }
    }
}