﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ToolLoan.Data.Models;

namespace ToolLoan.Models
{
    public class ReservedToolViewModel
    {
        public int userId { get; set; }
        public string username { get; set; }
        public List<Data.Models.Tool> tools { get; set; }
        public List<ReservedTool> reservedTools { get; set; }
        public bool isClean { get; set; }
    }
}