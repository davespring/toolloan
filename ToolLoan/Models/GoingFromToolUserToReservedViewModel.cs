﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ToolLoan.Models
{
    public class GoingFromToolUserToReservedViewModel
    {
        public int userId { get; set; }
        public string username { get; set; }
        public List<Data.Models.Tool> tools { get; set; }
    }
}
