﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ToolLoan.Data.Models;

namespace ToolLoan.Models
{
    public class RentToolViewModel
    {
        public List<User> Users { get; set; }
        public Tool ToolToRent { get; set; }

    }
}