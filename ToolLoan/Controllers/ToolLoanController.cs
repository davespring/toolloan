﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ToolLoan.Data.DAL;
using ToolLoan.Data.Models;

namespace ToolLoan.Controllers
{
    public class ToolLoanController : Controller
    {

        private const string UsernameKey = "ToolLoan_UserName";



        private readonly IUserDAL userDal;


        public ToolLoanController(IUserDAL userDal)
        {
            //Data.DAL.UserDAL theUserDal = new UserDAL();
            this.userDal = userDal;
            //this.userDal = theUserDal;
        }


        public ToolLoanController()
        {

        }

        /// <summary>
        /// Gets the value from the Session
        /// </summary>
        protected string CurrentUser
        {
            get
            {
                string username = string.Empty;

                //Check to see if user cookie exists, if not create it
                if (Session[UsernameKey] != null)
                {
                    username = (string)Session[UsernameKey];
                }

                return username;
            }
        }

        protected int CurrentUserId
        {
            get
            {
                string username = this.CurrentUser;
                int userId = 0;

                //Check to see if user cookie exists, if not create it
                if (username != String.Empty)
                {
                    userId = userDal.GetUserId(username);
                }

                return userId;
            }
        }
        


        /// <summary>
        /// "Logs" the current user in
        /// </summary>
        public void LogUserIn(string username)
        {
            Session[UsernameKey] = username;
        }


        /// <summary>
        /// "Logs out" a user by removing the cookie.
        /// </summary>
        public void LogUserOut()
        {
            Session.Abandon();
        }



        /// <summary>
        /// Returns bool if user has authenticated in
        /// </summary>        
        public bool IsAuthenticated
        {
            get
            {
                return Session[UsernameKey] != null;
            }
        }


        [ChildActionOnly]
        public ActionResult GetAuthenticatedUser()
        {
            User model = null;

            if (IsAuthenticated)
            {
                model = userDal.GetUser(CurrentUser);
            }

            return View("_AuthenticationBar", model);
        }

        [ChildActionOnly]
        public ActionResult AlwaysThere()
        {
            return View("_Footer");
        }

    }
}