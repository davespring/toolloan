﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ToolLoan.Data.DAL;
using ToolLoan.Data.Models;

namespace ToolLoan.Controllers
{
    
    public class ToolsController : ToolLoanController
    {
        private ToolLoanContext db = new ToolLoanContext();
        private readonly IUserDAL userDal;

        public ToolsController(IUserDAL userDal)
            : base(userDal)
        {
            this.userDal = userDal;
        }

        public ToolsController()
            :base()
        {

        }


        // GET: Tools
        public ActionResult Index()
        {
            if (base.IsAuthenticated)
            {
                return View(db.Tools.ToList());
            }
            else
            {
                return RedirectToAction("LogIn", "Account");
            }
            
        }

        public ActionResult LibrarianIndex()
        {
            if (base.IsAuthenticated)
            {
                return View(db.Tools.ToList());
            }
            else
            {
                return RedirectToAction("LogIn", "Account");
            }

        }

        // GET: Tools/Details/5
        public ActionResult Details(int? id)
        {
            if (base.IsAuthenticated)
            {
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                Tool tool = db.Tools.Find(id);
                if (tool == null)
                {
                    return HttpNotFound();
                }
                return View(tool);
            }
            else
            {
                return RedirectToAction("LogIn", "Account");
            }

        }

        // GET: Tools/Create
        public ActionResult Create()
        {
            if (base.IsAuthenticated)
            {
                return View();
            }
            else
            {
                return RedirectToAction("LogIn", "Account");
            }
            
        }

        // POST: Tools/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ToolId,Name,ToolCategoryId,TotalStock,Description,quantity")] Tool tool)
        {
            if (base.IsAuthenticated)
            {
                if (ModelState.IsValid)
                {
                    db.Tools.Add(tool);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }

                return View(tool);
            }
            else
            {
                return RedirectToAction("LogIn", "Account");
            }

        }

        // GET: Tools/Edit/5
        public ActionResult Edit(int? id)
        {
            if (base.IsAuthenticated)
            {
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                Tool tool = db.Tools.Find(id);
                if (tool == null)
                {
                    return HttpNotFound();
                }
                return View(tool);
            }
            else
            {
                return RedirectToAction("LogIn", "Account");
            }

        }

        // POST: Tools/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ToolId,Name,ToolCategoryId,TotalStock,Description,quantity")] Tool tool)
        {
            if (base.IsAuthenticated)
            {
                if (ModelState.IsValid)
                {
                    db.Entry(tool).State = EntityState.Modified;
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
                return View(tool);
            }
            else
            {
                return RedirectToAction("LogIn", "Account");
            }

        }

        // GET: Tools/Delete/5
        public ActionResult Delete(int? id)
        {
            if (base.IsAuthenticated)
            {
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                Tool tool = db.Tools.Find(id);
                if (tool == null)
                {
                    return HttpNotFound();
                }
                return View(tool);
            }
            else
            {
                return RedirectToAction("LogIn", "Account");
            }

        }

        // POST: Tools/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            if (base.IsAuthenticated)
            {
                Tool tool = db.Tools.Find(id);
                db.Tools.Remove(tool);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            else
            {
                return RedirectToAction("LogIn", "Account");
            }

        }


        // Dispose
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
