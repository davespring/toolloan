﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ToolLoan.Data.DAL;
using ToolLoan.Data.Models;
using ToolLoan.Models;

namespace ToolLoan.Controllers
{
    public class CheckoutController : ToolLoanController
    {


        private readonly IUserDAL userDal;
        private readonly ShoppingBagDAL shoppingBagDal = new ShoppingBagDAL();
        private ToolLoanContext db = new ToolLoanContext();

        public CheckoutController(IUserDAL userDal, ShoppingBagDAL shoppingBagDal)
            : base(userDal)
        {
            this.userDal = userDal;
            this.shoppingBagDal = shoppingBagDal;
        }

        public CheckoutController()
            :base()
        {

        }

        // GET: Checkout
        public ActionResult CheckoutConfirmation()
        {
            //List of tools that had a requested quantity too high
            List<Tool> ErrorTools = new List<Tool>();
            //Boolean to check if a redirect to the error page is needed
            bool needToRedirectToError = false;   
            //Creates a new order      
            var order = new Order();

            //order.Username = User.Identity.Name;
            order.Username = base.CurrentUser;
            order.OrderDate = DateTime.Now;

            // Save the Order
            db.Orders.Add(order);
            db.SaveChanges();

            // Process the Order
            var cart = ShoppingBagDAL.GetCart(this.HttpContext);

            // Decrement the stock of the tools from
            // the count of each tool in the cart
            var cartItems = cart.GetCartItems();
            foreach(var cartItem in cartItems)
            {
                var tool = db.Tools.Find(cartItem.Tool.ToolId);
                if(tool.TotalStock >= cartItem.Count)
                {
                    tool.TotalStock -= cartItem.Count;

                    //Add tool_user table logic here 
                    ToolUser newToolUser = new ToolUser();
                    int userId;
                    try
                    {
                        userId = db.Users.Where(x => x.UserName == CurrentUser).Select(x => x.Id).FirstOrDefault();
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                    newToolUser.userId = userId;
                    newToolUser.toolId = tool.ToolId;
                    db.ToolUser.Add(newToolUser);
                    db.SaveChanges();
                }
                else
                {
                    needToRedirectToError = true;
                    ErrorTools.Add(tool);                  
                }
                db.SaveChanges();
            }
            if (needToRedirectToError)
            {
                return View("ErrorFillingOrder", ErrorTools);
            }

            // Create teh order
            cart.CreateOrder(order);
            
            // Save the changes
            db.SaveChanges();

            return RedirectToAction("Complete", new { id = order.OrderId });
        }


        public ActionResult Complete(int id)
        {
            var currentUsername = base.CurrentUser;

            // Validate user with the order
            bool isValid = db.Orders.Any(
                o => o.OrderId == id &&
                o.Username == currentUsername);

            if (isValid)
            {
                return View(id);
            }
            else
            {
                return View("Error");
            }
        }

        public ActionResult ErrorFillingOrder(List<Tool> Tools)
        {
            var ErrorToolList = Tools;
            return View("ErrorFillingOrder",ErrorToolList);
        }

    }
}