﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ToolLoan.Data.DAL;
using ToolLoan.Models;

namespace ToolLoan.Controllers
{
    public class ShoppingCartController : ToolLoanController
    {

        //private readonly IShoppingCartDAL shoppingCartDal;
        private readonly IUserDAL userDal;
        private readonly ShoppingBagDAL shoppingBagDal = new ShoppingBagDAL();
        private ToolLoanContext db = new ToolLoanContext();

        public ShoppingCartController(IUserDAL userDal, ShoppingBagDAL shoppingBagDal)
            : base(userDal)
        {
            this.userDal = userDal;
            this.shoppingBagDal = shoppingBagDal;
        }

        public ShoppingCartController()
            :base()
        {

        }

        //// GET: ShoppingCart
        //public ActionResult AddToolToCart(int id)
        //{
        //    var userId = base.CurrentUserId;
        //    shoppingBagDal.AddToolToCart(userId, id);

        //    var userTools = shoppingBagDal.GetCart(userId);
        //    return View("Cart", userTools);
        //}


        // GET: /ShoppingCart/
        public ActionResult Index()
        {
            var cart = ShoppingBagDAL.GetCart(this.HttpContext);
            ////We have a username_cart table
            //Table.Add(cart.GetCartId);
            //Table.Add(base.CurrentUserId) 
            // Set up our ViewModel
            var viewModel = new ShoppingBagViewModel
            {
                CartItems = cart.GetCartItems(),
            };

            // Return the view
            return View(viewModel);
        }

        public ActionResult AddToolToCart(int id)
        {

            var tool = db.Tools.Find(id);

            // Get the current totalStock of the tool
            int totalStock = tool.TotalStock;

            if (totalStock > 0)
            {
                // Decrement TotalStock of Tool
                //tool.TotalStock = totalStock - 1;
                //db.SaveChanges();

                var cart = ShoppingBagDAL.GetCart(this.HttpContext);

                cart.AddToCart(tool);

                var model = shoppingBagDal.GetCartItems();
                
                return RedirectToAction("Index", "ShoppingCart");
            }
            else
            {
                return RedirectToAction("Index", "Tools");
            }

        }
    }
}