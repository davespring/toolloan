﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Script.Serialization;
using ToolLoan.Data.DAL;
using ToolLoan.Data.Models;
using ToolLoan.Models;

namespace ToolLoan.Controllers
{
    public class LibrarianReservationActionsController : ToolLoanController
    {


        private readonly IUserDAL userDal;
        private readonly ShoppingBagDAL shoppingBagDal = new ShoppingBagDAL();
        private ToolLoanContext db = new ToolLoanContext();

        public LibrarianReservationActionsController(IUserDAL userDal, ShoppingBagDAL shoppingBagDal)
            : base(userDal)
        {
            this.userDal = userDal;
            this.shoppingBagDal = shoppingBagDal;
        }

        public LibrarianReservationActionsController()
            : base()
        {

        }
        // GET: LibrarianReservationActions
        public ActionResult Index()
        {
            return View("Index");
        }
        [HttpPost]
        public ActionResult Index(string username)
        {
            int userId = 0;
            GoingFromToolUserToReservedViewModel viewModelForReservation = new Models.GoingFromToolUserToReservedViewModel();
            userId = db.Users.Where(x => x.UserName == username).Select(x => x.Id).FirstOrDefault();
            if (userId != 0)
            {
                var usersToolIds = db.ToolUser.Where(x => x.userId == userId).Select(x => x.toolId).ToList();
                List<Data.Models.Tool> usersTools = new List<Data.Models.Tool>();
                for (int i = 0; i < usersToolIds.Count; i++)
                {
                    var tool = db.Tools.Find(usersToolIds[i]);
                    usersTools.Add(tool);
                }
                viewModelForReservation.username = username;
                viewModelForReservation.userId = userId;
                viewModelForReservation.tools = usersTools;
                return View("ToolListForUser", viewModelForReservation);
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }

        public ActionResult ToolListForUser(GoingFromToolUserToReservedViewModel viewModelForReservation)
        {
            GoingFromToolUserToReservedViewModel userViewModelForReservation = new Models.GoingFromToolUserToReservedViewModel();
            return View("ToolListForUser", userViewModelForReservation);
        }


        public ActionResult ReserveToolsForUser(string username)
        {
            int userId = 0;

            List<Data.Models.Tool> usersTools = new List<Data.Models.Tool>();

            GoingFromToolUserToReservedViewModel viewModelForReservation = new Models.GoingFromToolUserToReservedViewModel();

            userId = db.Users.Where(x => x.UserName == username).Select(x => x.Id).FirstOrDefault();

            if (userId != 0)
            {
                var usersToolIds = db.ToolUser.Where(x => x.userId == userId).Select(x => x.toolId).ToList();

                for (int i = 0; i < usersToolIds.Count; i++)
                {
                    var tool = db.Tools.Find(usersToolIds[i]);
                    usersTools.Add(tool);
                }
                viewModelForReservation.username = username;
                viewModelForReservation.userId = userId;
                viewModelForReservation.tools = usersTools;
            }

            for (int i = 0; i < usersTools.Count; i++)
            {
                //Logic to add a tool and a user id to reservation table
                DateTime currentDate = new DateTime();
                currentDate = DateTime.Now;
                Data.Models.ReservedTool newReservedTool = new Data.Models.ReservedTool();
                newReservedTool.LoanStartDate = DateTime.Now;
                newReservedTool.LoanEndDate = DateTime.Now.AddDays(7);
                newReservedTool.ToolCategoryId = viewModelForReservation.tools[i].ToolCategoryId;
                newReservedTool.UserId = viewModelForReservation.userId;
                newReservedTool.ToolId = viewModelForReservation.tools[i].ToolId;
                db.ReservedTools.Add(newReservedTool);
                db.SaveChanges();

                //Take out all records of that userid in the tool_user table
                Data.Models.ToolUser toolUser = db.ToolUser.Where(x => x.userId == viewModelForReservation.userId).FirstOrDefault();
                db.ToolUser.Remove(toolUser);
                db.SaveChanges();
            }
            return View("ReserveToolsForUser", viewModelForReservation);
        }

        public ActionResult ReturnToolForm()
        {
            return View("ReturnToolForm");
        }

        public ActionResult ReturnTools(string username, string driverslicense, bool? isClean)
        {
            if (username != "")
            {
                int userId = 0;

                List<Data.Models.ReservedTool> usersTools = new List<Data.Models.ReservedTool>();

                ReservedToolViewModel viewModelForReturn = new Models.ReservedToolViewModel();
                if (username == null)
                {
                    userId = db.Users.Where(x => x.UserName == username).Select(x => x.Id).FirstOrDefault();
                }
                else
                {

                    userId = db.Users.Where(x => x.UserName == username).Select(x => x.Id).FirstOrDefault();
                }
                if (userId != 0)
                {
                    var usersToolIds = db.ReservedTools.Where(x => x.UserId == userId).Select(x => x.ToolId).ToList();

                    for (int i = 0; i < usersToolIds.Count; i++)
                    {
                        int returnToolId = usersToolIds[i];
                        var tool = db.ReservedTools.Where(r => r.ToolId == returnToolId).ToList().FirstOrDefault();
                        usersTools.Add(tool);
                    }
                    viewModelForReturn.username = username;
                    viewModelForReturn.userId = userId;
                    viewModelForReturn.reservedTools = usersTools;
                }
                return View("ReturnTools", viewModelForReturn);
            }
            else
            {
                List<Data.Models.ReservedTool> usersTools = new List<Data.Models.ReservedTool>();

                ReservedToolViewModel viewModelForReturn = new Models.ReservedToolViewModel();
                int userId;
                userId = db.Users.Where(x => x.DriversLicenseNumber == driverslicense).Select(x => x.Id).FirstOrDefault();
                User user = db.Users.Where(x => x.DriversLicenseNumber == driverslicense).FirstOrDefault();
                if (userId != 0)
                {
                    var usersToolIds = db.ReservedTools.Where(x => x.UserId == userId).Select(x => x.ToolId).ToList();

                    for (int i = 0; i < usersToolIds.Count; i++)
                    {
                        int returnToolId = usersToolIds[i];
                        var tool = db.ReservedTools.Where(r => r.ToolId == returnToolId).ToList().FirstOrDefault();
                        usersTools.Add(tool);
                    }
                    viewModelForReturn.username = user.UserName;
                    viewModelForReturn.userId = userId;
                    viewModelForReturn.reservedTools = usersTools;
                    return View("ReturnTools", viewModelForReturn);
                }
            }
            return View("ReturnToolsForm");
        }


        public ActionResult ReturnToolsConfirmation(int toolId, int userId, bool isClean)
        {


            List<decimal> feeList = new List<decimal>();
            decimal fee = 0;
            Tool Tool = db.Tools.Find(toolId);
            ReservedTool theToolsReservation = db.ReservedTools.Where(x => x.UserId == userId && x.ToolId == toolId).FirstOrDefault();
            bool isGas = Tool.ToolCategoryId == 2;
            bool isElectric = Tool.ToolCategoryId == 1;
            bool isHand = Tool.ToolCategoryId == 3;
            if (isGas)
            {
                fee += 2;
            }
            else if ((isElectric || isGas) && DateTime.Now > theToolsReservation.LoanEndDate)
            {
                int amountOfDays = 0;
                while (theToolsReservation.LoanEndDate != DateTime.Now)
                {
                    theToolsReservation.LoanEndDate.AddDays(1);
                    amountOfDays++;
                    fee += 1;
                }
            }
            else if (isHand && DateTime.Now > theToolsReservation.LoanEndDate)
            {
                int amountOfDays = 0;
                while (theToolsReservation.LoanEndDate != DateTime.Now)
                {
                    theToolsReservation.LoanEndDate.AddDays(1);
                    amountOfDays++;
                    fee += 1;
                }
            }
            else if (!isClean)
            {
                fee += 5;
            }
            Session["currentFee"] = fee;
            if (Session["totalFee"] == null)
            {
                Session["totalFee"] = (decimal)Session["currentFee"];
            }
            else
            {
                Session["totalFee"] = (decimal)Session["totalFee"] + (decimal)Session["currentFee"];
            }

            var theUsername = db.Users.Find(userId).UserName;





            ReservedTool toolToRemove = db.ReservedTools.Where(x => x.ToolId == toolId && x.UserId == userId).FirstOrDefault();

            db.ReservedTools.Remove(toolToRemove);
            db.SaveChanges();

            Tool toolToAddBack = db.Tools.Find(toolId);
            toolToAddBack.TotalStock += 1;
            db.SaveChanges();



            return RedirectToAction("ReturnTools", "LibrarianReservationActions", new { username = theUsername });

        }

        public ActionResult FeeConfirmation()
        {
            ToolLoan.Models.FeeViewModel FeeModel = new FeeViewModel();
            FeeModel.totalFee = (decimal)Session["totalFee"];
            return View("ReturnToolsConfirmation", FeeModel);
        }


        public ActionResult FormForToolAvailability()
        {
            var tools = db.Tools.Select(x => x.Name).ToList();
            return View("FormForToolAvailability", tools);
        }


        public ActionResult ToolAvailability(string name)
        {
            string toolName = name;
            DateTime latestDate = new DateTime();
            ToolAvailabilityViewModel viewModel = new Models.ToolAvailabilityViewModel();
            Tool Tool = db.Tools.Where(x => x.Name == toolName).FirstOrDefault();
            List<ReservedTool> toolList = db.ReservedTools.Where(x => x.ToolId == Tool.ToolId).ToList();
            if (Tool.TotalStock == 0)
            {
                latestDate = DateTime.Now.AddDays(7);
                for (int i = 0; i < toolList.Count; i++)
                {
                    if (toolList[i].LoanEndDate < latestDate)
                    {
                        latestDate = toolList[i].LoanEndDate;
                    }
                }
            }
            else
            {
                latestDate = DateTime.Now;
            }
            viewModel.Tool = Tool;
            viewModel.ToolsEarliestAvailability = latestDate;
            return View("ToolAvailability", viewModel);
        }


        public ActionResult MakeLibrarianAccountForm()
        {
            return View("MakeLibrarianAccountForm");
        }

        public ActionResult MakeLibrarianAccount(string username)
        {
            int userId = 0;
            userId = db.Users.Where(x => x.UserName == username).Select(x => x.Id).FirstOrDefault();
            User theUser = db.Users.Find(userId);
            theUser.IsLibrarian = true;
            db.SaveChanges();
            return View("MakeLibrarianAccount",theUser);
        }
    }
}
