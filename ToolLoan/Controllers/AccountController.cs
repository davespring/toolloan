﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.Mvc;
using ToolLoan.Data.DAL;
using ToolLoan.Data.Models;
using ToolLoan.Models;

namespace ToolLoan.Controllers
{
    public class AccountController : ToolLoanController
    {
        private ToolLoanContext db = new ToolLoanContext();
        private readonly IUserDAL userDal;
        

       

        public AccountController(IUserDAL userDal)
            : base(userDal)
        {
            this.userDal = userDal;
        }

        public AccountController()
            :base()
        {

        }


        [HttpGet]
        public ActionResult SignUp()
        {
            //var test = new Capstone.Data.Models.testing();
            //test.bool1 = isAuthenticated;
            //test.bool2 = isLibrarianAuthenticated;
            return View("SignUp");
        }

        [HttpPost]
        public ActionResult SignUp(ToolLoan.Data.Models.User user, string confirmPassword)
        {
            string username = "";
            username = db.Users.Where(x => x.UserName == user.UserName).Select(x => x.UserName).FirstOrDefault();
            if (username != null)
            {
                //Redirect to error instead 
                return RedirectToAction("UsernameAlreadyExists", "Account");
            }
            if (confirmPassword == user.Password && ModelState.IsValid)
            {
                var model = user;
                if (model != null)
                {
                    MD5CryptoServiceProvider md5Provider = new MD5CryptoServiceProvider();

                    byte[] source = ASCIIEncoding.ASCII.GetBytes(user.Password);
                    byte[] hash = md5Provider.ComputeHash(source);
                    string password = Convert.ToBase64String(hash);
                    byte[] byteSalt = ASCIIEncoding.ASCII.GetBytes(user.Password);
                    string salt = Convert.ToBase64String(byteSalt);
                    user.Password = password;
                    user.IsLibrarian = false;
                    user.Salt = salt;
                    //userDal.CreateUser(model, salt);

                    // Add the user and save the changes
                    db.Users.Add(user);
                    db.SaveChanges();

                    base.LogUserIn(user.UserName);
                    return RedirectToAction("SignUpConfirm", model);
                }
            }
            return View("SignUp");
        }


        public ActionResult SignUpConfirm(User user)
        {
            return View("SignUpConfirm", user);
        }


        [HttpGet]
        public ActionResult Login()
        {
            if (base.IsAuthenticated)
            {
                return RedirectToAction("Index", "Home", new { username = base.CurrentUser });
            }

            var model = new LoginViewModel();
  
            return View("Login", model);
        }



        [HttpPost]
        public ActionResult Login(LoginViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = userDal.GetUser(model.UserName, model.Password);

                // No user found with these credentials
                if (user == null)
                {
                    ViewBag.ErrorMessage = "The username or password provided is invalid";
                    return View("Login", model);
                }

                // Happy Path
                base.LogUserIn(user.UserName);


                return RedirectToAction("Index", "Home", new { username = user.UserName });
                
            }
            else
            {
                return View("Login", model);
            }
        }


        [HttpGet]
        public ActionResult Logout()
        {
            base.LogUserOut();

            return RedirectToAction("Index", "Home");
        }


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        public ActionResult UsernameAlreadyExists()
        {
            return View("UsernameAlreadyExists");
        }
    }
}
