﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ToolLoan.Data.Models;
using ToolLoan.Models;

namespace ToolLoan.Controllers
{
    public class HomeController : ToolLoanController
    {
        public ActionResult Index()
        {
            BooleanHolder thisOne = new BooleanHolder();

            
            if (base.IsAuthenticated)
            {
                thisOne.isAuthenticated = true;
            }

            else
            {
                thisOne.isAuthenticated = false;
            }

            return View("Index", thisOne);
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}