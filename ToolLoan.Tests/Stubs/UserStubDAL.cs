﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ToolLoan.Data.DAL;
using ToolLoan.Data.Models;

namespace ToolLoan.Tests.Stubs
{
    internal class UserStubDAL : IUserDAL
    {
        public User GetUser(string username)
        {
            User temp = new User();

            temp.Id = 7;
            temp.UserName = "FunWithTests";
            temp.Password = "Password0!";
            temp.DriversLicenseNumber = "ab123123";
            temp.PhoneNumber = "1234567890";

            if (username == temp.UserName)
            {
                return temp;
            }

            throw new Exception();
        }

        public User GetUser(string username, string password)
        {
            throw new NotImplementedException();
        }

        public int GetUserId(string username)
        {
            throw new NotImplementedException();
        }
    }
}
