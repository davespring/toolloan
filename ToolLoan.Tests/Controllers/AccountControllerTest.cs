﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ToolLoan.Controllers;
using System.Web.Mvc;
using ToolLoan.Data.DAL;

namespace ToolLoan.Tests.Controllers
{
    [TestClass]
    public class AccountControllerTest
    {
        [TestMethod]
        public void LogIn()
        {
            AccountController controller = new AccountController();

            ViewResult result = controller.Login() as ViewResult;

            Assert.AreEqual("Login", result.ViewName);
        }
    }
}
