﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using ToolLoan.Data.Models;

namespace ToolLoan.Data.DAL
{
    public class UserDAL : IUserDAL
    {
        private ToolLoanContext db = new ToolLoanContext();

        public User GetUser(string username, string password)
        {
            User user = new User();

            try
            {
                MD5CryptoServiceProvider md5Provider = new MD5CryptoServiceProvider();
                byte[] bytePass = ASCIIEncoding.ASCII.GetBytes(password);
                byte[] hash = md5Provider.ComputeHash(bytePass);
                string hashedPassword = Convert.ToBase64String(hash);

                user = db.Users.Where(x => x.UserName == username && x.Password == hashedPassword).FirstOrDefault();
            }
            catch(Exception ex)
            {
                throw ex;
            }

            return user;
        }


        public User GetUser(string username)
        {
            User user = new User();

            try
            {
                user = db.Users.Where(x => x.UserName == username).FirstOrDefault();
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return user;
        }

        public int GetUserId(string username)
        {
            int userId;

            try
            {
                userId = db.Users.Where(x => x.UserName == username).Select(x => x.Id).FirstOrDefault();
            }
            catch(Exception ex)
            {
                throw ex;
            }

            return userId;
        }
    }
}
