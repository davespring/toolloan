﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ToolLoan.Data.Models;

namespace ToolLoan.Data.DAL
{
    public interface IShoppingCartDAL
    {
        List<Tool> GetCart(int userId);
        void AddToolToCart(int userId, int toolId);
    }
}
