﻿using ToolLoan.Data.Models;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace ToolLoan.Data.DAL
{
    public class ToolLoanContext : DbContext
    {

        public ToolLoanContext() : base("ToolLoanContext")
        {
        }

        public DbSet<Tool> Tools { get; set; }
        public DbSet<User> Users { get; set; }       
        public DbSet<ReservedTool> ReservedTools { get; set; }
        public DbSet<Cart> Carts { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<OrderDetail> OrderDetails { get; set; }
        public DbSet<ToolUser> ToolUser { get; set; }
        

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }
    }
}