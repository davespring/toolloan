﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ToolLoan.Data.Models;

namespace ToolLoan.Data.DAL
{
    public class ShoppingCartDAL : IShoppingCartDAL
    {
        private ToolLoanContext db = new ToolLoanContext();

        public List<Tool> GetCart(int userId)
        {
            var user = db.Users.Find(userId);
            var userTools = user.Tools.ToList();
            return userTools;
        }


        public void AddToolToCart(int userId, int toolId)
        {
            var user = db.Users.Find(userId);
            var tool = db.Tools.Find(toolId);

            try
            {
                if(tool != null)
                {
                    user.Tools.Add(tool);
                    db.SaveChanges();
                }
            }
            catch(Exception ex)
            {
                throw ex;
            }

        }


    }
}
