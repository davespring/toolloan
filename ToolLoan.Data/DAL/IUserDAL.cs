﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ToolLoan.Data.Models;

namespace ToolLoan.Data.DAL
{
    public interface IUserDAL
    {
        User GetUser(string username, string password);
        User GetUser(string username);
        int GetUserId(string username);
    }
}
