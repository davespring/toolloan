﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ToolLoan.Data.Models
{
    public class Tool
    {
        public int ToolId { get; set; }
        public string Name { get; set; }
        public int ToolCategoryId { get; set; }
        public int TotalStock { get; set; }
        public string Description { get; set; }

        public virtual ICollection<User> Users { get; set; }

    }
}
