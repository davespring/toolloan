﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ToolLoan.Data.Models
{
    public class OrderDetail
    {
        public int OrderDetailId { get; set; }
        public int OrderId { get; set; }
        public int ToolId { get; set; }
        public int Quantity { get; set; }
        public string UserName { get; set; }
        public DateTime LoanStartDate { get; set; }
        public DateTime LoanEndDate { get; set; }
        public virtual Tool Tool { get; set; }
        public virtual Order Order { get; set; }
    }
}
