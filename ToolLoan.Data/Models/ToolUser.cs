﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ToolLoan.Data.Models
{
    public class ToolUser
    {
        [Key]
        public int toolUserId { get; set; }
        public int toolId { get; set; }
        public int userId { get; set; }

    }
}
